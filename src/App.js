import './App.css'
import { Table } from './Table'
import Add from './Add'

export default function App() {
  const columns = [
    { accessor: 'name', label: 'Name' },
    { accessor: 'age', label: 'Age' },
    { accessor : 'phone' , label :'Phone.no'},
    { accessor : 'gender' , label :'Gender'},
    
  ]

  const rows = [
    { id: 1, name: 'Liz ', age: 36, phone : '7895641230', gender: 'Male' },
    { id: 2, name: 'Jack ', age: 40, phone : '7895641230', gender: 'Male'},
    { id: 3, name: 'Tracy ', age: 39, phone : '7895641230', gender: 'Feeale' },
    { id: 4, name: 'Jenna ', age: 40, phone : '7895641230', gender: 'Female'},
    { id: 5, name: 'Kenneth ', age: 12, phone : '7895641230', gender: 'Female'},
    { id: 6, name: 'Pete ', age: 42, phone : '7895641230', gender: 'Male' },
    { id: 7, name: 'Frank ', age: 36, phone : '7895641230', gender: 'Male' },
    { id: 8, name: 'Lemon', age: 36, phone : '7895641230', gender: 'Male' },
    { id: 9, name: 'Donaghy', age: 40, phone : '7895641230', gender: 'Male'},
    { id: 10, name: ' Morgan', age: 39, phone : '7895641230', gender: 'Male' },
    { id: 11, name: 'Maroney', age: 40, phone : '7895641230', gender: 'Male'},
    { id: 12, name: 'Parcell', age: 52, phone : '7895641230', gender: 'Male'},
    { id: 13, name: 'Hornberger', age: 42, phone : '7895641230', gender: 'Male' },
    { id: 14, name: 'Rossitano', age: 36, phone : '7895641230', gender: 'Male' },
    { id: 15, name: 'Anjali', age: 36, phone : '7895641230', gender: 'Female' },
    { id: 16, name: 'Niharika', age: 40, phone : '7895641230', gender: 'Female'},
    { id: 17, name: 'Hitesha', age: 39, phone : '7895641230', gender: 'Female' },
    { id: 18, name: 'Sneha', age: 40, phone : '7895641230', gender: 'Female'},
    { id: 19, name: 'Muskkan', age: 54, phone : '7895641230', gender: 'Female'},
    { id: 20, name: 'Meera', age: 42, phone : '7895641230', gender: 'Female' },
    { id: 21, name: 'Aman', age: 36, phone : '7895641230', gender: 'Male' },
    { id: 22, name: 'Megha', age: 36, phone : '7895641230', gender: 'Female' },
    { id: 23, name: 'Pratik', age: 40, phone : '7895641230', gender: 'Male'},
    { id: 24, name: 'Dhwani', age: 39, phone : '7895641230', gender: 'Female' },
    { id: 25, name: 'Vishwas', age: 40, phone : '7895641230', gender: 'Male'},
    { id: 26, name: 'Shradha', age: 30, phone : '7895641230', gender: 'Female'},
    { id: 27, name: 'Pooonam', age: 42, phone : '7895641230', gender: 'Female' },
    { id: 28, name: 'Palak', age: 36, phone : '7895641230', gender: 'Female' },
    { id: 29, name: 'Monit', age: 36, phone : '7895641230', gender: 'Male' },
    { id: 30, name: 'Jack Rishi', age: 40, phone : '7895641230', gender: 'Male'},
    { id: 31, name: 'Shubhuu', age: 39, phone : '7895641230', gender: 'Male' },
    { id: 32 , name: 'Kiara', age: 40, phone : '7895641230', gender: 'Female'},
    { id: 33, name: 'Mayanak', age: 31, phone : '7895641230', gender: 'Male'},
    { id: 34, name: 'Neelam', age: 42, phone : '7895641230', gender: 'Female' },
    { id: 35, name: 'Franicooo', age: 36, phone : '7895641230', gender: 'Male' },
    { id: 36, name: 'Asheer', age: 36, phone : '7895641230', gender: 'Male' },
    { id: 37, name: 'Manori', age: 40, phone : '7895641230', gender: 'Female'},
    { id: 38, name: 'Abhishekh', age: 39, phone : '7895641230', gender: 'Male' },
    { id: 39, name: 'nidhi', age: 40, phone : '7895641230', gender: 'Female'},
    { id: 40, name: null, age: null, phone: null, gender: null },
  ]

  return (
    <div className="App">
      <Table rows={rows} columns={columns} />
    </div>
  )
}


